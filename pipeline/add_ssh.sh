#!/usr/bin/env bash

which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
eval "$(ssh-agent -s)"
ssh-add <(echo "$SSH_PRIV_KEY")
git config --global user.email "${GITLAB_USER_EMAIL}"
git config --global user.name "${GITLAB_USER_LOGIN}"
mkdir -p ~/.ssh
ssh-keyscan "${CI_SERVER_HOST}" >> gitlab-known-hosts
cat gitlab-known-hosts >> ~/.ssh/known_hosts
