#!/usr/bin/env bash
echo "=== Generating ENV file ==="
files=()
for entry in "$1"/*; do
  file_name="${entry##*/}"
  files+=("$file_name")
done

cd "$1" || exit
echo "$PWD"

rm .env || true
for value in "${files[@]}"; do
  upper="${value^^}"
  echo "${upper//-/}=$(cat "$value")" >>.env
done

echo "PROFILE=$(basename "$1")" >>.env
echo "=== Generating ENV file done ==="
cat ".env"
