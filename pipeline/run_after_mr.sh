#!/usr/bin/env bash

git switch master
git pull

if [[ "$1" =~ ^Merge\ branch\ \'feature\/dev_deploy\'\ into\ .*$ ]]; then
  echo "Valid pipeline job, should run Docker for DEV"
  ~/utilities/pipeline/deploy.sh dev
else
  if [[ "$1" =~ ^Merge\ branch\ \'feature\/test_deploy\'\ into\ .*$ ]]; then
    echo "Valid pipeline job, should run Docker for TEST"
    ~/utilities/pipeline/deploy.sh test
  else
    if [[ "$1" =~ ^Merge\ branch\ \'feature\/prod_deploy\'\ into\ .*$ ]]; then
      echo "Valid pipeline job, should run Docker for PROD"
      ~/utilities/pipeline/deploy.sh prod
    else
      echo "Invalid pipeline job" || exit
    fi
  fi
fi
