# todo: authorize user
# generate env file
~/utilities/pipeline/env_gen.sh "version/$1/"
# deploy
echo "=== Deploy to $1 ==="
docker-compose down
docker-compose --env-file "version/$1/.env" config
docker-compose -p elisa --env-file "version/$1/.env" up -d
echo "=== Deploy to $1 done ==="
