#!/usr/bin/env bash
# Extract the host where the server is running, and add the URL to the APIs
[[ $HOST =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/"
PROJECT="projects/${CI_PROJECT_ID}"
# Look which is the default branch
if [ -z "$1" ]; then
  REMOVE_BRANCH="true"
  echo ""
  echo "Get TARGET_BRANCH..."
  TARGET_BRANCH=$(curl "${HOST}${PROJECT}" -s --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(json.load(sys.stdin)['default_branch'])")
  echo ""
  echo "TARGET_BRANCH: ${TARGET_BRANCH}"
else
  REMOVE_BRANCH="false"
  TARGET_BRANCH="$1"
  echo ""
  echo "TARGET_BRANCH: ${TARGET_BRANCH}"
fi

if [ -z "$2" ]; then
  SOURCE_BRANCH="${CI_COMMIT_REF_NAME}"
else
  SOURCE_BRANCH="$2"
fi
echo ""
echo "SOURCE_BRANCH: ${SOURCE_BRANCH}"

MESSAGE="NAS - from ${SOURCE_BRANCH} to ${TARGET_BRANCH}"

# The description of our new MR, we want to remove the branch after the MR has
# been closed
if [ -z "$ASSIGNEE" ]; then
  BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${SOURCE_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": ${REMOVE_BRANCH},
    \"allow_collaboration\": true,
    \"title\": \"${MESSAGE}\"
}"
else
  echo ""
  echo "ASSIGNEE: ${ASSIGNEE} get ASSIGNEE_ID..." "URL: ${HOST}users?username=${ASSIGNEE}"
  ASSIGNEE_ID=$(curl -s "${HOST}users?username=${ASSIGNEE}" --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['id'])")
  echo ""
  echo "ASSIGNEE_ID: ${ASSIGNEE_ID}"
  BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${SOURCE_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": ${REMOVE_BRANCH},
    \"assignee_id\": \"${ASSIGNEE_ID}\",
    \"allow_collaboration\": true,
    \"title\": \"${MESSAGE}\"
    }"
fi

echo ""

# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LIST_MR=$(curl -s "${HOST}${PROJECT}/merge_requests?state=opened&scope=all" --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}")
COUNT_BRANCHES=$(echo "${LIST_MR}" | grep -o "\"source_branch\":\"${SOURCE_BRANCH}\"" | wc -l)

DIFF=$(curl -s "${HOST}${PROJECT}/repository/compare?from=${TARGET_BRANCH}&to=${SOURCE_BRANCH}" --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(len(json.load(sys.stdin)['diffs']))")
if [ "${DIFF}" -eq "0" ]; then
  echo "No diffs... No new merge request opened..."
  exit
fi

# No MR found, let's create a new one
if [ "${COUNT_BRANCHES}" -eq "0" ]; then
  echo "MR request body: ${BODY}"
  curl -X POST "${HOST}${PROJECT}/merge_requests" \
  --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
  --header "Content-Type: application/json" \
  --data "${BODY}"

  echo ""
  echo "Opened a new merge request: ${MESSAGE} and assigned to you"
  exit
fi

echo "No new merge request opened..."
