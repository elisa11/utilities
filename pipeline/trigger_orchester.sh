#!/usr/bin/env bash
echo "=== Make trigger for orchestration ==="
git config --global user.email "${GITLAB_USER_EMAIL}"
git config --global user.name "${GITLAB_USER_LOGIN}"
rm -rf "${DEPLOY_FOLDER}"
git clone git@"${CI_SERVER_HOST}":"${DEPLOY_PROJECT_PATH}".git
cd "${DEPLOY_FOLDER}" || exit
git fetch
git checkout -b "$BRANCH" "origin/$BRANCH" || git checkout -b "$BRANCH"
mkdir -p "$VERSION_DIR"
echo "$CI_COMMIT_SHORT_SHA" > "$VERSION_DIR/$CI_PROJECT_NAME"
git add .
git commit -m "$MESSAGE" || echo "No changes, nothing to commit!"
git push --set-upstream origin "$BRANCH"
echo "=== Make trigger for orchestration done ==="
