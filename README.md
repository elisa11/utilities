# Utilities

Projekt s utilitkami ako napriklad skripty pre pipeliny

## Pipeline scripty
### add_ssh.sh
Pridanie SSH klucov pre server

### trigger_orchester
Zapis verzie do **Docker deploy** projektu, nasledne vytvorenie MR po ktorom sa spusta *run_after_mr.sh*

### run_after_mr.sh
Rozhodovaci script po MR do mastru **Docker deploy** projektu, spusta *deploy.sh* pre pozadovane prostredie

### deploy.sh
Teardown a build stacku po aktualizacii **.env** scriptom *env_gen.sh*

### env_gen.sh
Generovanie **.env** environment suboru pre elisa stack podla ulozenych verzii v **Docker deploy**

### auto_merge_request.sh
Vytvorenie MR pri pushnuti do repa, pouzitie:

**HOST**=*${CI_PROJECT_URL}* ./auto_merge_request.sh *TARGET_BRANCH* *SOURCE_BRANCH*

- *CI_PROJECT_URL* - preddefinovana hodnota v gitlabe
- *TARGET_BRANCH* - kam mergujem
- *SOURCE_BRANCH* - odkial mergujem
- *ASSIGNEE* - username clena skupiny, ktoremu sa prideli MR. Je nutne zmenit podla riesitela v .gitlab-ci.yml daneho projektu

Curl cmd pre ziskanie userov z group Elisa11:
curl --silent "https://gitlab.com/api/v4/groups/9329356/members/all/" --header "PRIVATE-TOKEN: Lca2v_bz4Y4d-ncF_ogg"
